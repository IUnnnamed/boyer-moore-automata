package be.ac.umons.sdd.core;

import be.ac.umons.sdd.core.automaton.Automaton;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 1/08/13
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */
public class Utils {
    public static ArrayList<Word> generate_F(int m) {
        ArrayList<Word> toReturn = new ArrayList<Word>(m);
        if(m == 0) {
            return null;
        } else if (m == 1) {
            toReturn.add(Word.parseWord("a"));
            return toReturn;
        } else if (m == 2) {
            toReturn.add(Word.parseWord("aa"));
            toReturn.add(Word.parseWord("ba"));
            return toReturn;
        } else if (m == 3) {
            toReturn.add(Word.parseWord("aba"));
            toReturn.add(Word.parseWord("baa"));
            toReturn.add(Word.parseWord("bba"));
            return toReturn;
        } else {
            long min = 1L << m - 3;
            long max = 1L << m;

            for(long i = min; i < max; i += 2) {
                long temp = i;
                boolean error = false;
                for (int j = 0; j < m; j++) {
                    if (temp == 0 || (temp & 7) != 0) {
                        temp >>>= 1;
                    }
                    else {
                        error = true;
                        break;
                    }
                }
                if (!error) {
                    toReturn.add(new Word(i,(byte) m));
                }
            }
        }
        return toReturn;
    }

    public static int taille(Word word) {
        Automaton a = new Automaton(word);
        a.process();
        return a.getSize();
    }
}
