package be.ac.umons.sdd.core.automaton;

import be.ac.umons.sdd.core.Word;
import be.ac.umons.sdd.core.tree.RBTree;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 4/08/13
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */
public class Automaton {
    private Word word;
    private RBTree tree;
    public Automaton(Word word) {
        this.word = word;
        tree = new RBTree();
    }

    public void process() {
        this.process(new State(this.word));
    }

    public int getSize() {
        return tree.size();
    }

    private void process(State state) {
        if (state != null && !tree.contains(state)) {
            tree.add(state);
            this.process(state.transitionA());
            this.process(state.transitionB());
        }
    }
}
