package be.ac.umons.sdd.core.automaton;

import be.ac.umons.sdd.core.Word;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 3/08/13
 * Time: 18:23
 * To change this template use File | Settings | File Templates.
 */
public class State implements Comparable<State> {
    private Word word;
    private long discoveredBit;

    public State(Word word) {
        this.word = word;
        this.discoveredBit = 0L;
    }

    public State(Word word, long discoveredBit) {
        this.word = word;
        this.discoveredBit = discoveredBit;
    }

    public Word getWord() {
        return this.word;
    }

    public long getDiscoveredBit() {
        return this.discoveredBit;
    }

    public State transitionA() {
        if (!this.isFinal()) {
            long rightmost = 1 << this.firstBlank();
            if ((word.getValue() & rightmost) == 0) {
                return new State(word, (discoveredBit | rightmost));
            } else {
                long p = discoveredBit | rightmost;
                byte s = this.shift(p, 'a');
                return new State(word, getNewP(p, s));
            }
        }

        return null;
    }

    private long getNewP(long p, byte s) {
        p = (p & ((1 << (this.word.getSize() - s)) - 1) ) <<  s;

        return p;
    }

    public State transitionB() {
        if (!this.isFinal()) {
            long rightmost = 1 << this.firstBlank();
            if ((word.getValue() & rightmost) == rightmost) {
                return new State(word, (discoveredBit | rightmost));
            } else {
                long p = discoveredBit | rightmost;
                byte s = this.shift(p, 'b');
                return new State(word, getNewP(p, s));
            }
        }

        return null;
    }

    public Word getDiscoveredWord() {
        return new Word(word.getValue() & discoveredBit, word.getSize());
    }

    public byte firstBlank() {
        long temp = this.discoveredBit;
        for (byte i = 0; i < word.getSize(); i++) {
            if ((temp & 1) == 0) {
                return i;
            } else {
                temp >>>= 1;
            }
        }
        return word.getSize();
    }

    public byte shift(long p, char c) {
        boolean error;
        for (byte s = 1; s < word.getSize(); s++) {
            error = false;
            for (byte k = s; k < word.getSize(); k++) {
                long t = 1 << k;
                if ((t & p) == t) {
                    long t2 = 1 << (k - s);
                    if (t != this.firstBlank()) {
                        if ((word.getValue() & t) != (word.getValue() & t2)) {
                            error = true;
                            break;
                        }
                    } else {
                        if ((word.getValue() & t2) != ((c == 'a')? 0: t2)) {
                            error = true;
                            break;
                        }
                    }
                }
            }

            if (!error) {
                return s;
            }
        }

        return word.getSize();
    }

    public boolean isFinal() {
        return discoveredBit == (1 << this.word.getSize()) - 1;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(word.getSize());
        long temp1 = word.getValue();
        long temp2 = discoveredBit;
        for (byte i = 0; i < word.getSize(); i++) {
            if ((temp2 & 1) == 1) {
                sb.append(((temp1 & 1) == 0)? 'a' : 'b');
            } else {
                sb.append('.');
            }
            temp1 >>>= 1;
            temp2 >>>= 1;
        }

        return sb.reverse().toString();
    }

    public int compareTo(State other) {
        long value = this.word.getValue() + this.discoveredBit;
        long otherValue = other.getWord().getValue() + other.getDiscoveredBit();
        return (value < otherValue)? -1 : (value == otherValue)? 0 : 1;
    }
}
