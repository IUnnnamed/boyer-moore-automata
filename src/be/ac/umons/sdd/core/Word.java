package be.ac.umons.sdd.core;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 11/08/13
 * Time: 16:30
 * To change this template use File | Settings | File Templates.
 */
public class Word {
    private byte size;
    private long word;

    public Word(long word, byte size) {
        this.word = word;
        this.size = size;
    }

    public static Word parseWord(String string) {
        long word = 0;
        byte size = (byte) string.length();
        for (byte i = 0; i < size; i++) {
            word <<= 1;
            word |= (string.charAt(i) == 'b')? 1 : 0;
        }

        return new Word(word, size);
    }

    public byte getSize() {
        return this.size;
    }

    public long getValue() {
        return this.word;
    }

    public boolean equals(Object w) {
        if (w instanceof Word) {
            return this.size == ((Word) w).getSize() && this.word == ((Word) w).getValue();
        }

        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(size);
        long temp = word;
        for (byte i = 0; i < size; i++) {
            sb.append(((temp & 1) == 0)? 'a' : 'b');
            temp >>>= 1;
        }

        return sb.reverse().toString();
    }
}
