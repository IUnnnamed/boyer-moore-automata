package be.ac.umons.sdd.core.tree;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 2/08/13
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
public class RBNode extends Node {
    public static final int RED = 0;
    public static final int BLACK = 1;
    private int color;
    private RBNode p;

    public RBNode(Comparable data) {
        super(data);
    }

    public RBNode(Comparable data, int color){
        super(data);
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public RBNode getParent() {
        return this.p;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setParent(RBNode p) {
        this.p = p;
    }
}
