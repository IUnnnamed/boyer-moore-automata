package be.ac.umons.sdd.core.tree;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 2/08/13
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public interface SearchTree {
    int size();
    void add(Comparable data);
    void remove (Comparable data);
    boolean contains(Comparable data);
}
