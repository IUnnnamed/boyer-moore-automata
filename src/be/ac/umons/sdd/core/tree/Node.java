package be.ac.umons.sdd.core.tree;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 2/08/13
 * Time: 14:47
 * To change this template use File | Settings | File Templates.
 */
public class Node implements INode{
    private Comparable data;
    private INode left;
    private INode right;

    public Node(Comparable data) {
        this.data = data;
    }

    public Node() {
        this.data = null;
    }

    public Comparable getData() {
        return this.data;
    }

    public void setData(Comparable data) {
        this.data = data;
    }

    public void setRight(INode right) {
        this.right = right;
    }

    public void setLeft(INode left) {
        this.left = left;
    }

    public INode getLeft() {
        return this.left;
    }

    public INode getRight() {
        return this.right;
    }
}
