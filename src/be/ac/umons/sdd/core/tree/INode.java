package be.ac.umons.sdd.core.tree;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 2/08/13
 * Time: 17:27
 * To change this template use File | Settings | File Templates.
 */
public interface INode {
    public Comparable getData();

    public void setData(Comparable data);

    public void setRight(INode right);
    public void setLeft(INode left);

    public INode getLeft();
    public INode getRight();
}
