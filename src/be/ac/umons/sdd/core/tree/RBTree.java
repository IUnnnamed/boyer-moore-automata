package be.ac.umons.sdd.core.tree;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 2/08/13
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
public class RBTree implements SearchTree {
    private RBNode root;
    private static final RBNode nil = new RBNode(null, RBNode.BLACK);
    private int size;

    public RBTree() {
        this.root = RBTree.nil;
        this.root.setParent(RBTree.nil);
        this.root.setLeft(RBTree.nil);
        this.root.setRight(RBTree.nil);
        size = 0;
    }
    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void add(Comparable data) {
        this.add(new RBNode(data));
    }

    private void add(RBNode node) {
        boolean contains = false;
        RBNode y = RBTree.nil;
        RBNode x = this.root;
        while(x != RBTree.nil) {
            y = x;
            if (node.getData().compareTo(x.getData()) < 0) {
                x = (RBNode) x.getLeft();
            } else if (node.getData().compareTo(x.getData()) > 0){
                x = (RBNode) x.getRight();
            } else {
                contains = true;
            }
        }

        if (!contains) {
            node.setParent(y);

            if (y == RBTree.nil) {
                this.root = node;
            } else if (node.getData().compareTo(y.getData()) < 0) {
                y.setLeft(node);
            } else if (node.getData().compareTo(y.getData()) > 0) {
                y.setRight(node);
            }
            node.setLeft(RBTree.nil);
            node.setRight(RBTree.nil);
            node.setColor(RBNode.RED);
            insertFix(node);
            size++;
        }
    }

    private void insertFix(RBNode node) {
        while (node.getParent().getColor() == RBNode.RED) {
            if (node.getParent() == node.getParent().getParent().getLeft()) {
                RBNode y = (RBNode) node.getParent().getParent().getRight();
                if (y.getColor() == RBNode.RED) {
                    node.getParent().setColor(RBNode.BLACK);
                    y.setColor(RBNode.BLACK);
                    node.getParent().getParent().setColor(RBNode.RED);
                    node = node.getParent().getParent();
                } else {
                    if (node == node.getParent().getRight()) {
                        node = node.getParent();
                        this.leftRotate(node);
                    }

                    node.getParent().setColor(RBNode.BLACK);
                    node.getParent().getParent().setColor(RBNode.RED);
                    this.rightRotate(node.getParent().getParent());
                }
            } else {
                RBNode y = (RBNode) node.getParent().getParent().getLeft();
                if (y.getColor() == RBNode.RED) {
                    node.getParent().setColor(RBNode.BLACK);
                    y.setColor(RBNode.BLACK);
                    node.getParent().getParent().setColor(RBNode.RED);
                    node = node.getParent().getParent();
                } else {
                    if (node == node.getParent().getLeft()) {
                        node = node.getParent();
                        this.rightRotate(node);
                    }

                    node.getParent().setColor(RBNode.BLACK);
                    node.getParent().getParent().setColor(RBNode.RED);
                    this.leftRotate(node.getParent().getParent());
                }
            }
        }
        this.root.setColor(RBNode.BLACK);
    }

    private void leftRotate(RBNode node) {
        RBNode y = (RBNode) node.getRight();
        node.setRight(y.getLeft());
        if (y.getLeft() != RBTree.nil) {
            ((RBNode) y.getLeft()).setParent(node);
        }
        y.setParent(node.getParent());

        if (node.getParent() == RBTree.nil) {
            this.root = y;
        } else if (node == node.getParent().getLeft()) {
            node.getParent().setLeft(y);
        } else {
            node.getParent().setRight(y);
        }

        y.setLeft(node);
        node.setParent(y);
    }

    private void rightRotate(RBNode node) {
        RBNode y = (RBNode) node.getLeft();
        node.setLeft(y.getRight());
        if (y.getRight() != RBTree.nil) {
            ((RBNode) y.getRight()).setParent(node);
        }
        y.setParent(node.getParent());

        if (node.getParent() == RBTree.nil) {
            this.root = y;
        } else if (node == node.getParent().getLeft()) {
            node.getParent().setLeft(y);
        } else {
            node.getParent().setRight(y);
        }

        y.setRight(node);
        node.setParent(y);
    }

    @Override
    public void remove(Comparable data) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean contains(Comparable data) {
        RBNode x = this.root;
        while (x != RBTree.nil) {
            if (data.compareTo(x.getData()) == 0) {
                return true;
            } else if (data.compareTo(x.getData()) < 0) {
                x = (RBNode) x.getLeft();
            } else {
                x = (RBNode) x.getRight();
            }
        }

        return false;
    }
}
