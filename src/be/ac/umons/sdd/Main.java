package be.ac.umons.sdd;

import be.ac.umons.sdd.core.CpuTime;
import be.ac.umons.sdd.core.Utils;
import be.ac.umons.sdd.core.Word;
import be.ac.umons.sdd.core.automaton.Automaton;
import be.ac.umons.sdd.core.automaton.State;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Patrick
 * Date: 1/08/13
 * Time: 16:51
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args) {
	    CpuTime time = new CpuTime();
        Scanner sc = new Scanner(System.in);
        System.out.print("Veuillez indiquer la longueur des mots a generer : ");
        int m = sc.nextInt();
	    time.start();
        ArrayList<Word> fm = Utils.generate_F(m);
        process(fm);
	    time.stop();
        System.out.printf("\nTemps de calcul (%s)\n", time.getTime());
    }

    public static void process(ArrayList<Word> list) {
        ArrayList<Word> toReturn = new ArrayList<Word>();
        int max = Utils.taille(list.get(0));
        toReturn.add(list.get(0));

        int current = 0;
        for (int i = 1; i < list.size(); i++) {
            current = Utils.taille(list.get(i));
            if (max < current) {
                max = current;
                toReturn.clear();
                toReturn.add(list.get(i));
            } else if (max == current) {
               toReturn.add(list.get(i));
            }
        }

        System.out.printf("Mots de taille %d generant un automate de taille maximal t = %d\n", list.get(0).getSize(), max);
        System.out.println("\nListe des mots:");
        print(toReturn);
    }

    public static void print(ArrayList<Word> out) {
        for (Word s: out) {
            System.out.printf(s + "\n");
        }
    }
}
